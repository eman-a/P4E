fName = input('Insert File name: ')
fHandle = open(fName, 'r')
total = 0

count = 0
for line in fHandle:
    line = line.rstrip()
    if line.startswith('X-DSPAM-Confidence'):
        pos =  line.find('0')
        lineFloat = line[pos:]
        lineFloat = float(lineFloat)
        total = total + lineFloat
        count = count + 1

print(total / count)
